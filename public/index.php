<?php

use App\Controllers\FrontController;

require "../vendor/autoload.php";

$frontController = new FrontController();
$frontController->Dispatch();
