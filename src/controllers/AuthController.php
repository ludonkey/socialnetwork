<?php

namespace App\Controllers;

use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use App\Models\UserManager;

class AuthController
{
    private static $JWT_KEY = 'my_secret_key';

    private $_isApi;
    private $_userId;

    private $_userManager;

    public function __construct($isApi)
    {
        $this->_isApi = $isApi;
        $this->_userManager = new UserManager();

        if ($this->_isApi) {
            $idFromToken = $this->GetUserIdFromToken();
            if ($idFromToken === false) {
                $this->_userId = null;
            } else if ($idFromToken > 0) {
                $this->_userId = $idFromToken;
            }
        } else {
            session_start();
            if (isset($_SESSION["userId"])) {
                $this->_userId = $_SESSION["userId"];
            }
        }
    }

    public function GetUserId()
    {
        return $this->_userId;
    }

    public function CheckLogged()
    {
        if (is_null($this->_userId)) {
            http_response_code(400);
            if ($this->_isApi) {
                include "../views/Api/ApiForbiddenPath.php";
            } else {
                include "../views/ForbiddenPath.php";
            }
            return false;
        }
        return true;
    }

    public function LogIn($params)
    {
        if (isset($params["username"]) && isset($params["password"])) {
            $user = $this->_userManager->GetOneUserFromLoginAndPassword($params["username"], $params["password"]);
            if ($user === false || !isset($user['id'])) {
                $errorMsg = "Wrong login and/or password";
                if ($this->_isApi) {
                    include "../views/api/ApiLoginForm.php";
                } else {
                    include "../views/LoginForm.php";
                }
            } else {
                $this->_userId = $user['id'];
                if ($this->_isApi) {
                    $token = $this->GenerateToken($this->_userId);
                    include "../views/api/ApiLoginForm.php";
                } else {
                    $_SESSION["userId"] = $this->_userId;
                    header("location:/display");
                }
            }
        } else {
            if ($this->_isApi) {
                include "../views/api/ApiLoginForm.php";
            } else {
                include "../views/LoginForm.php";
            }
        }
    }

    public function LogOut($params)
    {
        if ($this->_isApi) {
            header("location:/api/display");
        } else {
            session_destroy();
            header("location:/display");
        }
    }

    public function Register($params)
    {
        if (isset($params['username']) && isset($params['password']) && isset($params['passwordRetype'])) {
            $errorMsg = NULL;
            if (!$this->_userManager->IsNicknameFree($params['username'])) {
                $errorMsg = "Nickname already used.";
            } else if ($params['password'] != $params['passwordRetype']) {
                $errorMsg = "Passwords are not the same.";
            } else if (strlen(trim($params['password'])) < 8) {
                $errorMsg = "Your password should have at least 8 characters.";
            } else if (strlen(trim($params['username'])) < 4) {
                $errorMsg = "Your nickame should have at least 4 characters.";
            }
            if ($errorMsg) {
                if ($this->_isApi) {
                    include "../views/api/ApiRegisterForm.php";
                } else {
                    include "../views/RegisterForm.php";
                }
            } else {
                $userId = $this->_userManager->CreateNewUser($params['username'], $params['password']);
                if ($this->_isApi) {
                    $this->_userId = $userId;
                    $token = $this->GenerateToken($this->_userId);
                    include "../views/api/ApiLoginForm.php";
                } else {
                    $this->_userId = $userId;
                    $_SESSION['userId'] = $this->_userId;
                    header("location:/display");
                }
            }
        } else {
            if ($this->_isApi) {
                $errorMsg = "You should provide username, password & passwordRetype.";
                include "../views/api/ApiRegisterForm.php";
            } else {
                include "../views/RegisterForm.php";
            }
        }
    }

    private function GenerateToken($userId)
    {
        $data = [
            "iss" => $_SERVER['SERVER_NAME'],
            "iat" => time(),
            "nbf" => time() + 1,
            "exp" => time() + 60 * 30,
            "user" => array(
                "id" => $userId
            )
        ];

        return JWT::encode($data, AuthController::$JWT_KEY, 'HS256');
    }

    private function GetUserIdFromToken()
    {
        $headers = apache_request_headers();
        if (!isset($headers['Authorization'])) {
            return 0;
        }
        $authHeader = $headers['Authorization'];
        $arr = explode(" ", $authHeader);
        if (!isset($arr[1])) {
            return false;
        }
        try {
            $token = $arr[1];
            $data = JWT::decode($token, new Key(AuthController::$JWT_KEY, 'HS256'));
            if (isset($data) && isset($data->user) && isset($data->user->id)) {
                return $data->user->id;
            }
        } catch (Exception $e) {
        }
        return false;
    }
}
