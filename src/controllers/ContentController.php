<?php

namespace App\Controllers;

use App\Models\PostManager;
use App\Models\CommentManager;

class ContentController
{
    private $_isApi;
    private $_userId;
    private $_postManager;
    private $_commentManager;

    public function __construct($userId, $isApi)
    {
        $this->_userId = $userId;
        $this->_isApi = $isApi;
        $this->_postManager = new PostManager();
        $this->_commentManager = new CommentManager();
    }

    public function Display($params)
    {
        if (isset($params["search"])) {
            $posts = $this->_postManager->SearchInPosts($params['search']);
        } else {
            $posts = $this->_postManager->GetAllPosts();
        }
        $comments = array();
        foreach ($posts as $onePost) {
            $idPost = $onePost['id'];
            $commentsForOnePost = $this->_commentManager->GetAllCommentsFromPostId($idPost);
            $comments[$idPost] = $commentsForOnePost;
        }
        if ($this->_isApi) {
            include "../views/api/ApiDisplayPosts.php";
        } else {
            include "../views/DisplayPosts.php";
        }
    }

    public function NewMsg($params)
    {
        if (isset($params["msg"])) {
            $msg = $params["msg"];
            $this->_postManager->CreateNewPost($msg, $this->_userId);
        }
        if ($this->_isApi) {
            header("location:/api/display");
        } else {
            header("location:/display");
        }
    }

    public function CreateNewComment($params)
    {
        if (isset($params['postId']) && isset($params['comment'])) {
            $postId = $params['postId'];
            $comment = $params['comment'];
            $this->_commentManager->CreateNewComment($this->_userId, $postId, $comment);
        }
        if ($this->_isApi) {
            header("location:/api/display");
        } else {
            header("location:/display");
        }
    }
}
