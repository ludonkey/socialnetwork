<?php

namespace App\Controllers;

use App\Controllers\AuthController;
use App\Controllers\ContentController;

class FrontController
{

  public function Dispatch()
  {
    $action = $this->GetAction($isApi, $params);
    $this->InjectHeaders($isApi);

    $authController = new AuthController($isApi);
    $contentController = new ContentController($authController->GetUserId(), $isApi);

    switch ($action) {

      case 'register':
        $authController->Register($params);
        break;

      case 'login':
        $authController->LogIn($params);
        break;

      case 'logout':
        if (!$authController->CheckLogged()) {
          die;
        }
        $authController->LogOut($params);
        break;

      case 'newMsg':
        if (!$authController->CheckLogged()) {
          die;
        }
        $contentController->NewMsg($params);
        break;

      case 'newComment':
        if (!$authController->CheckLogged()) {
          die;
        }
        $contentController->CreateNewComment($params);
        break;

      case 'display':
      default:
        $contentController->Display($params);
        break;
    }
  }

  private function GetAction(&$isApi, &$params)
  {
    $isApi = false;
    $params = array();
    $action = substr(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), 1);
    if (str_starts_with($action, "api")) {
      $isApi = true;
      $action = substr($action, 3);
      if (str_starts_with($action, "/")) {
        $action = substr($action, 1);
      }
      $params = json_decode(file_get_contents("php://input"), true);
    } else {
      $params = array_merge($_GET, $_POST);
    }
    return $action;
  }

  private function InjectHeaders($isApi)
  {
    if ($isApi) {
      header("Access-Control-Allow-Origin: * ");
      header("Content-Type: application/json; charset=UTF-8");
      header("Access-Control-Allow-Methods: POST");
      header("Access-Control-Max-Age: 3600");
      header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    }
  }
}
