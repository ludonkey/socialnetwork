<?php

namespace App\Models;

use PDO;
use App\Models\DBManager;

class CommentManager
{

  public function GetOneCommentFromId($id)
  {
    $response = DBManager::PDO()->query("SELECT * FROM comment WHERE id = $id");
    return $response->fetch(PDO::FETCH_ASSOC);
  }

  public function GetAllComments()
  {
    $response = DBManager::PDO()->query("SELECT * FROM comment ORDER BY created_at ASC");
    return $response->fetchAll(PDO::FETCH_ASSOC);
  }

  public function GetAllCommentsFromUserId($userId)
  {

    $response = DBManager::PDO()->query(
      "SELECT comment.*, user.nickname "
        . "FROM comment LEFT JOIN user on (comment.user_id = user.id) "
        . "WHERE comment.user_id = $userId "
        . "ORDER BY comment.created_at ASC"
    );
    return $response->fetchAll(PDO::FETCH_ASSOC);
  }

  public function GetAllCommentsFromPostId($postId)
  {
    $response = DBManager::PDO()->query(
      "SELECT comment.*, user.nickname "
        . "FROM comment LEFT JOIN user on (comment.user_id = user.id) "
        . "WHERE comment.post_id = $postId "
        . "ORDER BY comment.created_at ASC"
    );
    return $response->fetchAll(PDO::FETCH_ASSOC);
  }

  public function CreateNewComment($userId, $postId, $comment)
  {
    $sql = "INSERT INTO comment(user_id, post_id, content) values (:userId, :postId, :comment)";
    $stmt = DBManager::PDO()->prepare($sql);
    $stmt->execute(
      array(
        "userId" => $userId,
        "postId" => $postId,
        "comment" => $comment
      )
    );
  }
}
