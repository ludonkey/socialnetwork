<?php

namespace App\Models;

use PDO;
use App\Models\DBManager;

class PostManager
{

  public function GetOnePostFromId($id)
  {
    $response = DBManager::PDO()->query("SELECT * FROM post WHERE id = $id");
    return $response->fetch(PDO::FETCH_ASSOC);
  }

  public function GetAllPosts()
  {
    $response = DBManager::PDO()->query(
      "SELECT post.*, user.nickname "
        . "FROM post LEFT JOIN user on (post.user_id = user.id) "
        . "ORDER BY post.created_at DESC"
    );
    return $response->fetchAll(PDO::FETCH_ASSOC);
  }

  public function GetAllPostsFromUserId($userId)
  {
    $response = DBManager::PDO()->query("SELECT * FROM post WHERE user_id = $userId ORDER BY created_at DESC");
    return $response->fetchAll(PDO::FETCH_ASSOC);
  }

  public function SearchInPosts($stringToSearch)
  {
    $response = DBManager::PDO()->query(
      "SELECT post.*, user.nickname "
        . "FROM post LEFT JOIN user on (post.user_id = user.id) "
        . " WHERE content like '%$stringToSearch%' "
        . " ORDER BY post.created_at DESC"
    );
    return $response->fetchAll(PDO::FETCH_ASSOC);
  }

  public function CreateNewPost($msg, $userId)
  {
    $sql = "INSERT INTO post(content, user_id) values(:msg, :userId)";
    $stmt = DBManager::PDO()->prepare($sql);
    $stmt->execute(
      array(
        "userId" => $userId,
        "msg" => $msg
      )
    );
  }
}
