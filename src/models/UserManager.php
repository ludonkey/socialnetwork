<?php

namespace App\Models;

use PDO;
use App\Models\DBManager;

class UserManager
{

  public function GetOneUserFromId($id)
  {
    $response = DBManager::PDO()->query("SELECT * FROM user WHERE id = $id");
    return $response->fetch(PDO::FETCH_ASSOC);
  }

  public function GetAllUsers()
  {
    $response = DBManager::PDO()->query("SELECT * FROM user ORDER BY nickname ASC");
    return $response->fetchAll(PDO::FETCH_ASSOC);
  }

  public function GetOneUserFromLoginAndPassword($login, $password)
  {
    $stmt = DBManager::PDO()->prepare("SELECT * FROM user WHERE nickname = :login and password = :password");
    $resultat = $stmt->execute(
      array(
        "login" => $login,
        "password" => $password
      )
    );
    if ($resultat == false) {
      return null;
    }
    return $stmt->fetch(PDO::FETCH_ASSOC);
  }

  public function IsNicknameFree($nickname)
  {
    $response = DBManager::PDO()->prepare("SELECT * FROM user WHERE nickname = :nickname ");
    $response->execute(
      array(
        "nickname" => $nickname
      )
    );
    return $response->rowCount() == 0;
  }

  public function CreateNewUser($nickname, $password)
  {
    $response = DBManager::PDO()->prepare("INSERT INTO user (nickname, password) values (:nickname , :password )");
    $response->execute(
      array(
        "nickname" => $nickname,
        "password" => $password
      )
    );
    return DBManager::PDO()->lastInsertId();
  }
}
