<?php

namespace App\Models;

use PDO;

class DBManager
{

    private static $_connection;

    public static function PDO()
    {
        if (!is_null(self::$_connection)) {
            return self::$_connection;
        }
        require_once dirname(__FILE__) . "/../../config/database.php";
        $db_dataSourceName = "mysql:host=$db_host;port=$db_port;dbname=$db_name";

        self::$_connection = new PDO($db_dataSourceName, $db_user, $db_passwd);
        self::$_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return self::$_connection;
    }
}
