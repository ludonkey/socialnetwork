<?php

if (isset($posts)) {
  for ($i = 0; $i < count($posts); $i++) {
    $postId = $posts[$i]['id'];
    if (isset($comments[$postId])) {
      $posts[$i]["comments"] = $comments[$postId];
    } else {
      $posts[$i]["comments"] = array();
    }
  }
  echo json_encode($posts);
}
