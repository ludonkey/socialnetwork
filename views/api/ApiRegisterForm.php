<?php
if (isset($token)) {
  echo '{ "jwt": "' . $token . '"}';
} else {
  http_response_code(400);
  if (isset($errorMsg)) {
    echo '{ "error": "' . $errorMsg . '"}';
  }
}
