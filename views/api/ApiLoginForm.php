<?php
if (isset($token)) {
  echo '{ "jwt": "' . $token . '"}';
} else {
  http_response_code(400);
  if (isset($errorMsg)) {
    echo '{ "error": "' . $errorMsg . '"}';
  } else {
    echo '{ "error": "Please send username and password"}';
  }
}
